const theme = require('./src/theme')

module.exports = {
  siteMetadata: {
    title: 'redpwnCTF 2021',
    description: 'redpwnCTF is a cybersecurity competition hosted by the redpwn CTF team. It’s online, jeopardy-style, and includes a wide variety of challenges.',
    author: '@redpwnctf',
    imageUrl: 'https://redpwn.storage.googleapis.com/branding/redpwnctf-og.png',
    origin: 'https://ctf.redpwn.net'
  },
  plugins: [
    'gatsby-plugin-react-helmet',
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'images',
        path: `${__dirname}/src/images`
      }
    },
    'gatsby-transformer-sharp',
    'gatsby-plugin-sharp',
    {
      resolve: 'gatsby-plugin-manifest',
      options: {
        name: 'redpwnCTF',
        short_name: 'redpwnCTF',
        start_url: '/',
        background_color: theme.accent,
        theme_color: theme.accent,
        display: 'minimal-ui',
        icon: 'src/images/icon.png' // This path is relative to the root of the site.
      }
    },
    'gatsby-plugin-theme-ui'
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ]
}
