module.exports = {
  env: {
    es6: true,
    browser: true
  },
  extends: [
    'standard',
    'plugin:react/recommended',
    'plugin:react-hooks/recommended'
  ],
  parserOptions: {
    ecmaFeatures: {
    },
    ecmaVersion: 2018,
    sourceType: 'module'
  },
  plugins: [
  ],
  rules: {
    'no-multiple-empty-lines': ['error', {
      max: 1,
      maxEOF: 0,
      maxBOF: 0
    }],
    'react/jsx-no-target-blank': ['error', {
      allowReferrer: true
    }]
  },
  settings: {
    react: {
      version: 'detect'
    }
  }
}
