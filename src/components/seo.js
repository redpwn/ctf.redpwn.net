/**
 * SEO component that queries for data with
 *  Gatsby's useStaticQuery React hook
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import React from 'react'
import PropTypes from 'prop-types'
import { Helmet } from 'react-helmet'
import { useStaticQuery, graphql } from 'gatsby'
import theme from '../theme'

function SEO ({ description, lang, meta, title: _title }) {
  const { site } = useStaticQuery(
    graphql`
      query {
        site {
          siteMetadata {
            title
            description
            author
            imageUrl
            origin
          }
        }
      }
    `
  )

  const metaDescription = description || site.siteMetadata.description
  const metaImageUrl = site.siteMetadata.imageUrl
  const metaOrigin = site.siteMetadata.origin

  const title = _title || site.siteMetadata.title

  return (
    <Helmet
      htmlAttributes={{
        lang
      }}
      defaultTitle={site.siteMetadata.title}
      title={_title || undefined}
      titleTemplate={`%s | ${site.siteMetadata.title}`}
      meta={[
        {
          name: 'title',
          content: title
        },
        {
          name: 'description',
          content: metaDescription
        },
        {
          property: 'og:title',
          content: title
        },
        {
          property: 'og:description',
          content: metaDescription
        },
        {
          property: 'og:type',
          content: 'website'
        },
        {
          property: 'og:image',
          content: metaImageUrl
        },
        {
          proprty: 'og:url',
          content: metaOrigin
        },
        {
          name: 'twitter:card',
          content: 'summary_large_image'
        },
        {
          name: 'twitter:creator',
          content: site.siteMetadata.author
        },
        {
          name: 'twitter:title',
          content: title
        },
        {
          name: 'twitter:image',
          content: metaImageUrl
        },
        {
          name: 'twitter:url',
          content: metaOrigin
        },
        {
          name: 'twitter:description',
          content: metaDescription
        },
        {
          name: 'theme-color',
          content: theme.colors.background
        }
      ].concat(meta)}
    />
  )
}

SEO.defaultProps = {
  lang: 'en',
  meta: [],
  description: ''
}

SEO.propTypes = {
  description: PropTypes.string,
  lang: PropTypes.string,
  meta: PropTypes.arrayOf(PropTypes.object),
  title: PropTypes.string
}

export default SEO
