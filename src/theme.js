module.exports = {
  fonts: {
    body: 'system-ui, sans-serif',
    heading: '"Montserrat", sans-serif',
    monospace: '"Roboto Mono", monospace'
  },
  fontWeights: {
    body: 400,
    heading: 700,
    bold: 700
  },
  lineHeights: {
    body: 1.4,
    heading: 1.125
  },
  colors: {
    background: '#111',
    text: '#eee',
    muted: '#777',
    primary: '#ef3838',
    accent: '#b5191d'
  },
  radii: ['0.25rem', '0.5rem', '1rem', '2rem'],
  shadows: [
    '0 0 0.5rem 0 rgba(250, 250, 250, 0.25)',
    '0 0 0.5rem 0 rgba(250, 250, 250, 0.5)',
    '0 0 0.75rem 0 rgba(250, 250, 250, 0.5)'
  ],
  text: {
    heading: {
      marginBlockEnd: 3
    },
    paragraph: {
      ':not(:last-child)': {
        marginBlockEnd: 2
      }
    }
  },
  styles: {
    root: {
      fontFamily: 'body',
      fontWeight: 'body'
    },
    a: {
      color: 'primary'
    },
    ul: {
      margin: 0,
      ':not(:last-child)': {
        marginBlockEnd: 2
      }
    }
  },
  cards: {
    primary: {
      color: 'text',
      bg: 'background',
      padding: 4,
      borderRadius: 2,
      boxShadow: 1
    }
  }
}
